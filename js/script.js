$(document).ready(function() {
    var days = $('.count #first');
    var daysVal = parseInt(days.text());

    var timer = setTimeout(function tick() {
    if (daysVal > 0) {
        $('.count #first').text(daysVal - 1);
        $('.count #second').text(daysVal);
        $('.count #third').text(daysVal - 1);
        $('.count #forth').text(daysVal);

        $('.count #first').addClass('animated');
        $('.count #second').addClass('animated');
        $('.count #third').addClass('animated');
        $('.main-top .inner .count .top').addClass('animated');
        --daysVal;
        timer = setTimeout(tick, 5000);
    }else{
        $('.count #first').removeClass('animated');
        $('.count #second').removeClass('animated');
        $('.count #second').text(0);
        $('.count #forth').text(0);
        $('.count #third').removeClass('animated');
        $('.main-top .inner .count .top').removeClass('animated');
    }
    }, 5000);

    //scroll to second screen
    $('.button .arrow').click(function(){
        $('body, html').animate({
            scrollTop: $('#bottom').offset().top
        }, 2000)
    });


});